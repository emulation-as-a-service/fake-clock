// Copyright 2019 The Emulation-as-a-Service Authors
// SPDX-License-Identifier: CC0-1.0

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define MAX_CLOCKS 16  // from <linux/time.h>
#define START_TIME (uint64_t)1e8
#define MAX_FORWARD_JUMP_SEC 10

static int (*_clock_gettime)();
static struct timespec clocks[MAX_CLOCKS] = {{0, 0}, {START_TIME, 0}};
static struct timespec offsets[MAX_CLOCKS] = {0};

__attribute__((unused)) static int timespeclt(struct timespec a,
                                              struct timespec b) {
  return a.tv_sec != b.tv_sec ? a.tv_sec < b.tv_sec : a.tv_nsec < b.tv_nsec;
}

static struct timespec timespecadd(struct timespec a, struct timespec b) {
  return a.tv_nsec + b.tv_nsec < (uint64_t)1e9
             ? (struct timespec){a.tv_sec + b.tv_sec, a.tv_nsec + b.tv_nsec}
             : (struct timespec){a.tv_sec + b.tv_sec + 1,
                                 a.tv_nsec + b.tv_nsec - (uint64_t)1e9};
}

static struct timespec timespecsub(struct timespec a, struct timespec b) {
  return a.tv_nsec >= b.tv_nsec
             ? (struct timespec){a.tv_sec - b.tv_sec, a.tv_nsec - b.tv_nsec}
             : (struct timespec){a.tv_sec - b.tv_sec - 1,
                                 a.tv_nsec - b.tv_nsec + (uint64_t)1e9};
}

static void _clock_gettime_init() {
  _clock_gettime = dlsym(RTLD_NEXT, "clock_gettime");
}

int clock_gettime(clockid_t clk_id, struct timespec *tp) {
  if(!_clock_gettime)
    _clock_gettime_init();

  int ret = _clock_gettime(clk_id, tp);
  if (ret || clk_id >= MAX_CLOCKS || clk_id != CLOCK_MONOTONIC) return ret;

  struct timespec tp_orig = *tp;
  struct timespec tp_ret = timespecadd(tp_orig, offsets[clk_id]);
  struct timespec diff = timespecsub(tp_ret, clocks[clk_id]);
  if (diff.tv_sec < 0) {
    offsets[clk_id] = timespecsub(clocks[clk_id], tp_orig);
    tp_ret = clocks[clk_id];
    fprintf(stderr, "LD_PRELOAD_clock_gettime: detected backward jump, "
      "new offset: %li s\n", offsets[clk_id].tv_sec);
  } else if (diff.tv_sec >= MAX_FORWARD_JUMP_SEC) {
    tp_ret = timespecadd(clocks[clk_id], (struct timespec) {1, 0});
    offsets[clk_id] = timespecsub(tp_ret, tp_orig);
    fprintf(stderr, "LD_PRELOAD_clock_gettime: detected forward jump, "
      "new offset: %li s\n", offsets[clk_id].tv_sec);
  }
  clocks[clk_id] = tp_ret;
  *tp = tp_ret;
  return ret;
}

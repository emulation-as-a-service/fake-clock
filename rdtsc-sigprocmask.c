#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

/**
 * Called with:
 *
 *   LD_PRELOAD=./LD_PRELOAD_rdtsc.so ./rdtsc-sigprocmask
 *
 * this will crash as the SIGSEGV handler in LD_PRELOAD_rdtsc.so cannot run.
 */

int main() {
  sigset_t sigset;
  sigemptyset(&sigset);
  sigaddset(&sigset, SIGSEGV);
  sigprocmask(SIG_BLOCK, &sigset, 0);
  for (;;) {
    uint64_t tsc;
    uint32_t high, low;
    asm("rdtsc" : "=d"(high), "=a"(low));
    tsc = (uint64_t)high << 32 | low;
    printf("%" PRIu64 "\n", tsc);
    sleep(1);
  }
}

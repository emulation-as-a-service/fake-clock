#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>

int main() {
  for (;;) {
    uint64_t tsc;
    uint32_t high, low;
    asm("rdtsc" : "=d"(high), "=a"(low));
    tsc = (uint64_t)high << 32 | low;
    printf("%" PRIu64 "\n", tsc);
    sleep(1);
  }
}

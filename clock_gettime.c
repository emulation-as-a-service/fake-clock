#include <stdio.h>
#include <time.h>
#include <unistd.h>

int main() {
  for (;;) {
    struct timespec time;
    clock_gettime(CLOCK_MONOTONIC, &time);
    printf("%li %li\n", time.tv_sec, time.tv_nsec);
    sleep(1);
  }
}

// Copyright 2019 The Emulation-as-a-Service Authors
// SPDX-License-Identifier: CC0-1.0

#define _GNU_SOURCE
#include <dlfcn.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/prctl.h>
#include <time.h>
#include <ucontext.h>

static uint64_t tsc = 1234567890;

static uint64_t rdtsc() {
  return tsc++;
  struct timespec time;
  prctl(PR_SET_TSC, PR_TSC_ENABLE);
  clock_gettime(CLOCK_MONOTONIC, &time);
  prctl(PR_SET_TSC, PR_TSC_SIGSEGV);
  return (uint64_t)time.tv_sec * (1000 * 1000 * 1000) + time.tv_nsec;
}

static void signal_segv(int signum, siginfo_t *siginfo, void *ptr) {
  ucontext_t *ucontext = ptr;
  struct sigcontext *mcontext = (struct sigcontext *)&ucontext->uc_mcontext;
  if (*(uint16_t *)(mcontext->rip) == 0x310f) {
    uint64_t ret = rdtsc();
    mcontext->rdx = ret >> 32;
    mcontext->rax = (uint32_t)ret;
    mcontext->rip += 2;
  }
}

#define PRELOAD(sym, sym_t)                                  \
  static sym_t (*_##sym)();                                  \
  __attribute__((constructor)) static void _##sym##_init() { \
    _##sym = dlsym(RTLD_NEXT, #sym);                         \
  }

PRELOAD(pthread_sigmask, int);
int pthread_sigmask(int how, const sigset_t *set, sigset_t *oldset) {
  sigdelset((sigset_t *)set, SIGSEGV);
  return _pthread_sigmask(how, set, oldset);
}

__attribute__((constructor)) static void _rdtsc_init() {
  // static char stack[SIGSTKSZ];
  // sigaltstack(&(stack_t){.ss_sp = stack, .ss_size = sizeof stack}, 0);
  struct sigaction sa = {.sa_sigaction = signal_segv,
                         .sa_flags = SA_SIGINFO /* | SA_ONSTACK */};
  // TODO: Chain-call old signal handler
  // TODO: Also override sigaction/signal to catch new signal handlers
  struct sigaction old = {0};
  sigaction(SIGSEGV, &sa, &old);
  prctl(PR_SET_TSC, PR_TSC_SIGSEGV);
}

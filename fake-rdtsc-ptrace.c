#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

static struct user *const user = 0;

uint64_t rdtsc() {
  struct timespec time;
  clock_gettime(CLOCK_MONOTONIC, &time);
  return (uint64_t)time.tv_sec * (uint64_t)1e9 + time.tv_nsec;
}

int handle_instruction(pid_t pid) {
  unsigned long rip = ptrace(PTRACE_PEEKUSER, pid, &user->regs.rip);
  unsigned long inst = ptrace(PTRACE_PEEKTEXT, pid, rip);
  if ((inst & 0xffff) == 0x310f) {  // rdtsc
    rip += 2;
    ptrace(PTRACE_POKEUSER, pid, &user->regs.rip, rip);
    uint64_t ret = rdtsc();
    ptrace(PTRACE_POKEUSER, pid, &user->regs.rdx, ret >> 32);
    ptrace(PTRACE_POKEUSER, pid, &user->regs.rax, (uint32_t)ret);
    return 1;
  }
  return 0;
}

int main(int argc, char **argv) {
  pid_t child = fork();
  if (!child) {
    prctl(PR_SET_TSC, PR_TSC_SIGSEGV);
    ptrace(PTRACE_TRACEME);
    kill(getpid(), SIGSTOP);
    return execvp(argv[1], argv + 1);
  }
  waitpid(child, 0, 0);
  ptrace(PTRACE_SETOPTIONS, child, 0,
         PTRACE_O_TRACECLONE | PTRACE_O_TRACEFORK | PTRACE_O_TRACEVFORK |
             PTRACE_O_TRACESYSGOOD);
  ptrace(PTRACE_CONT, child, 0, 0);
  for (;;) {
    int stat;
    pid_t pid = wait(&stat);
    if (pid == -1) return stat;
    // printf("stat %x %i\n", stat, WSTOPSIG(stat));
    if (WIFSTOPPED(stat) && !(WSTOPSIG(stat) & ~0x7f)) {
      // printf("stat %x %i\n", stat, WSTOPSIG(stat));
      int sig = WSTOPSIG(stat);
      if (WSTOPSIG(stat) == SIGSEGV && handle_instruction(pid)) {
        ptrace(PTRACE_CONT, pid, 0, 0);
        continue;
      }
      if (sig == 5) sig = 0;
      ptrace(PTRACE_CONT, pid, 0, sig);
      continue;
    }
    ptrace(PTRACE_CONT, pid, 0, 0);
  }
}
